<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('post/dataprofesional', 'BetterController@PostProfesional');
Route::get('getprofesional', 'BetterController@GetDataProfesional');
Route::get('/buscar/profesional','BetterController@BusquedaProfesional');
Route::put('/update/dataprofesional','BetterController@UpdateProfesional');
Route::delete('/deleteprofesional/{id_profesional}','BetterController@DeleteProfesional');
