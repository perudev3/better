<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblProfesional extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_profesional', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idProfesional');            
            $table->string('nombre_apellidos')->nullable();
            $table->string('cedula')->nullable();
            $table->date('fecha_nacimiento')->nullable();
            $table->string('profesion')->nullable();
            $table->string('direccion')->nullable();
            $table->string('municipio')->nullable();
            $table->string('telefono')->nullable();
            $table->string('sexo')->nullable();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_profesional');
    }
}
