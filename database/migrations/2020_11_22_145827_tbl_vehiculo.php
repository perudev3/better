<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblVehiculo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_vehiculo', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idVehiculo');
            $table->integer('id_profesional')->unsigned();
            $table->string('categoria')->nullable();
            $table->string('marca')->nullable();
            $table->string('año')->nullable();
            $table->timestamps();
            $table->foreign('id_profesional')->references('idProfesional')->on('tbl_profesional');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_vehiculo');
    }
}
