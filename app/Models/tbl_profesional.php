<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tbl_profesional extends Model
{
    protected $table='tbl_profesional';

    protected $fillable = ['nombre_apellidos','cedula', 'fecha_nacimiento','profesion','direccion', 'municipio', 'telefono', 'sexo'];

    protected $primaryKey = 'idProfesional';

    public $timestamps = false;

    protected $hidden = ['remember_token'];

    
}
