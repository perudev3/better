<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tbl_vehiculo extends Model
{
    protected $table='tbl_vehiculo';

    protected $fillable = ['id_profesional', 'categoria','marca','año'];

    protected $primaryKey = 'idVehiculo';

    public $timestamps = false;

    protected $hidden = ['remember_token'];


    public function profesional(){
        return $this->belongsTo('App\Models\tbl_profesional','id_profesional','idProfesional');
    }

}
